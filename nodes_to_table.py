#!/usr/bin/env python

import ete3
import argparse
import logging
from pathlib import Path


ncbi = ete3.NCBITaxa()

def parse_arguments():
    parser=argparse.ArgumentParser(
            description="A utility to produce a 3-column tsv file that "
            "partitions all leaves as `in_leaves` if they are descendants "
            "of a node and `out_leaves` otherwise."
            )

    optionalArgs = parser._action_groups.pop()
    optionalArgs.title = "Optional Arguments"

    requiredArgs = parser.add_argument_group("Required Arguments")

    requiredArgs.add_argument(
            "-i",
            "--input",
            type=lambda p: Path(p).resolve(strict=True),
            help="Path to newick formatted tree",
            dest="input",
            required=True,
            )
    
    requiredArgs.add_argument(
            "-o",
            "--output",
            type=lambda p: Path(p).resolve(),
            help="Path to output tsv",
            dest="output",
            required=True,
            )
    
    optionalArgs.add_argument(
            "--format",
            type=int,
            help="Format of the input newick tree [ default = 0 ]",
            default=0,
            dest="format",
            required=False
            )

    optionalArgs.add_argument(
            "--no-header",
            help="Don't write a header in the output_tsv",
            default=True,
            action="store_false",
            dest="header",
            required=False
            )

    optionalArgs.add_argument(
            "-v",
            "--verbose",
            help="Print more messages for debugging",
            required=False,
            action="store_true",
            dest="verbose",
           )


    parser._action_groups.append(optionalArgs)

    return parser.parse_args()


def label_internal_nodes(tree):
    '''
    Assign names to internal nodes.

    Given a tree with unlabeled internal nodes,
    produce a new tree where nodes are labeled as 
    `I<N>` , where N is a based on a counter for 
    all nodes.

    Positional arguments
      tree: ete3.Tree : Input tree

    Return
      tree: ete3.Tree : A tree with labeled
        nodes and topology unchanged
    '''
    counter = 1

    for node in tree.traverse():
        if node.is_leaf() is False:
            node_name = f'I{counter}'
            node.name = node_name
            counter += 1

    return tree


def partition_leaves(labeled_tree):
    '''
    Create a dict with in/out leaves for each node
    
    Root node gets a '-' entry for outer leaves.

    Positional arguments
      labeled_tree: ete3.Tree: A tree with named internal
        nodes

    Return
      leaves_dic: dict: A nested dic with outer
        keys the node names and in/out leaves stored
        as sets under keys 'in_leaves' and 
        'out_leaves' i.e.
        { node_name : 
          { 'in_leaves' : { l1, l2, l3, ...},
            'out_leaves': { l5, l8, l9, ...}
          },
            ...
        }
    '''
    
    leaves_dic = {}

    all_leaves = set([leaf.name for leaf in labeled_tree.get_leaves()])

    for node in labeled_tree.traverse():
        if node.is_leaf() is False:
            in_leaves = set([l.name for l in node.get_leaves()])
            out_leaves = all_leaves.difference(in_leaves)

            if len(out_leaves) == 0:
                out_leaves = '-'

            leaves_dic[node.name] = { 
                    'in_leaves' : sorted(in_leaves),
                    'out_leaves' : sorted(out_leaves),
                    }

    return leaves_dic


def get_taxids_from_labels(labels_set):
    """Create a list of taxids from the provided leaf labels

    Assumes format '\.*|(taxid)\..*'

    Positional arguments
      labels_set: set: A set of leaf labels, each one of the format 

    Return
      taxids: list: A list of 
    """
    taxids = []
    for label in labels_set:
        taxid = label.split('|')[1].split('.')[0]
        taxids.append(taxid)
    return taxids


def get_root_label(tree):
    """
    Positional arguments
      tree: ete3.Tree: A tree

    Return
      root_name: str: String of the root node name
    """
    root_tree = tree.get_tree_root()
    for node in root_tree.traverse():
        if node.is_root():
            root_name = node.name
            break
    return root_name


def get_taxonomy_lca_for_leaves(leaves, ncbi):
    """Get taxonomy information for the ancestor of leaves
    """
    taxids = get_taxids_from_labels(leaves)
    ncbi_taxids = ncbi.get_taxid_translator(taxids)
    ncbi_topology = ncbi.get_topology(ncbi_taxids.keys())

    lca_taxid = get_root_label(ncbi_topology)
    # get_taxid_translator returns a dict
    lca_dic = ncbi.get_taxid_translator([lca_taxid])
    lca_name = lca_dic[int(lca_taxid)]

    # get_rank returns a dict
    lca_rank_dic = ncbi.get_rank([lca_taxid])
    lca_rank = lca_rank_dic[int(lca_taxid)]

    return lca_name, lca_rank


def update_leaves_dic_with_ancestor(leaves_dic, ncbi):
    """Append ancestor information to leaves parttitions

    Positional arguments
      leaves_dic: dict: A dictionary as returned from partition_leaves
      ncbi: ete3.NCBITaxa() obj: An instance of the NCBITaxa() provided 
        by ete3

    Return:
      leaves_dic: dict: An updated dictionary for each node that now 
        holds 'in_lca', 'in_rank', 'out_lca', 'out_rank' as keys and
        their values retrieved from ncbi
    """

    for node in leaves_dic:
        in_leaves = leaves_dic[node]['in_leaves']
        out_leaves = leaves_dic[node]['out_leaves']

        in_lca, in_rank = get_taxonomy_lca_for_leaves(in_leaves, ncbi)

        if list(out_leaves)[0] == '-':
            out_lca, out_rank = '-', '-' 
        else:
            out_lca, out_rank = get_taxonomy_lca_for_leaves(out_leaves, ncbi)
        
        leaves_dic[node]['in_lca'] = in_lca
        leaves_dic[node]['in_rank'] = in_rank
        leaves_dic[node]['out_lca'] = out_lca
        leaves_dic[node]['out_rank'] = out_rank

    return leaves_dic        


def write_leaves_table(leaves_dic, outfile, header=True):
    '''
    Write the output tsv file from a given leaves_dic
    
    Positional arguments
      leaves_dic: dict: A dict as returned from update_leaves_dic_with_ancestor
      outfile: pathlib.Path obj: Path to output file
      header: bool: Write a header or not

    Return
      None
     
    '''
    header_fields = ['node', 
            'in_leaves', 
            'out_leaves', 
            'in_lca', 
            'in_rank', 
            'out_lca', 
            'out_rank',
            ]

    header_line = '\t'.join(header_fields)

    with open(outfile, 'w') as fout:
        if header is True:
            fout.write(header_line + '\n')

        for node in leaves_dic:
            in_leaves_string = ';'.join(leaves_dic[node]['in_leaves'])
            out_leaves_string = ';'.join(leaves_dic[node]['out_leaves'])
            in_lca = leaves_dic[node]['in_lca']
            in_rank = leaves_dic[node]['in_rank']
            out_lca = leaves_dic[node]['out_lca']
            out_rank = leaves_dic[node]['out_rank']


            fout.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.
                    format(
                        node,
                        in_leaves_string, 
                        out_leaves_string,
                        in_lca,
                        in_rank,
                        out_lca,
                        out_rank
                        )
                    )
    return 


def suffix_nw(outpath):
    '''
    Create a suffixed filename based on given path 

    Positional arguments
      outpath: pathlib.Path obj: The provided path to 
        the output table file

    Return
      suffixed_path: pathlib.Path obj: A path with 
        the last suffix of the given outpath replaced 
        with '_processed_tree.nw'
    '''
    original_suffix = outpath.suffix
    outdir = outpath.parents[0]
    basename = outpath.name.replace(original_suffix, '.processed_tree.nw')
    suffixed_path = outdir / Path(basename)
    return suffixed_path


def main():
    args = parse_arguments()

    # logging redundant
    loglevel = logging.INFO
    if args.verbose:
        loglevel = logging.DEBUG
    logging.basicConfig(
            format='[ %(asctime)s ] - %(levelname)s - %(message)s',
            level=loglevel
            ) 
    

    ncbi = ete3.NCBITaxa()

    logging.debug(msg = "Reading tree from file: {}".format(args.input))

    tree = ete3.Tree(str(args.input), format=args.format)
    
    labeled_tree = label_internal_nodes(tree)

    leaves_partitions = partition_leaves(labeled_tree)

    leaves_partitions = update_leaves_dic_with_ancestor(leaves_partitions, ncbi)

    write_leaves_table(leaves_partitions, args.output, header=args.header)

    # Write a new newick with labels used here 
    newick_out = suffix_nw(args.output)
    tree.write(outfile=str(newick_out), format=1)

    logging.debug(msg = "Processed newick output: {}".format(newick_out))


if __name__ == '__main__':
    main()

