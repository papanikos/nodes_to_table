# nodes_to_table

A utility to create a tsv file containing some leaf information for a given 
phylogenetic tree.


## Requirements

- `python 3.x`
- `ete3 >= 3.1.2`

## Installation

None. 

* The script is executable so you might want to include the directory 
where it is located in your `$PATH` variable.

* The `ete3.NCBITaxa()` class is used to get taxonomy information about the 
ancestral nodes of the leaves. The first time you run the script and if you 
haven't run `ete3.NCBITaxa()` before on your machine, all required files by 
ete3 are downloaded and stored in the `$HOME/.etetoolkit` directory. You 
will see some related output printed on your screen. This is done only once 
and the database will be there for all future invocations.

## Usage

```
usage: nodes_to_table.py [-h] -i INPUT -o OUTPUT [--format FORMAT] [--no-header] [-v]

A utility to produce a 3-column tsv file that partitions all leaves as `in_leaves` if they are descendants of a node and `out_leaves` otherwise.

Required Arguments:
  -i INPUT, --input INPUT
                        Path to newick formatted tree
  -o OUTPUT, --output OUTPUT
                        Path to output tsv

Optional Arguments:
  -h, --help            show this help message and exit
  --format FORMAT       Format of the input newick tree [ default = 0 ]
  --no-header           Don't write a header in the output_tsv
  -v, --verbose         Print more messages for debugging

```
## Input

A valid phylogenetic tree in newick format **with unlabeled internal nodes**.

Input with named internal nodes can be processed with the option `--format 1`, 
but the names will be rewritten in the resulting `*.processed_tree.nw`. 
The topology doesn't change, but now you know why the output has `I1`, `I2` 
etc.

## Output

- `<output.tsv>`

The main output file, can be named whatever you want. Leaf names are `;` 
separated.

- `output.processed_tree.nw`

A newick tree with internal node names included (aka format `1` in 
ETE-ish). This is also written in the same directory as the output
table. The name of the file is derived from the given output file name. 
This is to make sure we know what node we are talking about, when the input 
has no internal node names. The original topology remains the same.

For an example input and output see the [test](./test) directory. 

Files in there were produced with

```
$ nodes_to_table.py -i test/test.ph -o test/output.tsv
```

